import torch
import setup.meta as m

def load(rundir):
    topology = m.load(rundir, 'topology.json')
    edges = topology['edges']
    topology['edges'] = { int(rank):edges[rank] for rank in edges }
    topology['weights'] = torch.tensor(topology['weights'])
    if 'neighbourhoods' in topology.keys():
        ns = topology['neighbourhoods']
        topology['neighbourhoods'] = { int(rank):ns[rank] for rank in ns }
    return topology

# Takes a list of nodes with sample ranges and returns a new list
# for the same nodes with proportions [0,1] for the classes
def class_proportions(nodes):
    def sample_nb(r):
        return r[1]-r[0]
    def total(rs):
        return sum(map(sample_nb, rs))
    return [ [ float(sample_nb(r))/total(node['samples']) for r in node['samples'] ]  for node in nodes ]

