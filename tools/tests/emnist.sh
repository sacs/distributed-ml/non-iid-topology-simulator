#!/usr/bin/env bash
# Path to current script
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
# Add current working directory to executable namespace
export PATH=$PATH:$SCRIPT_DIR/../
# Setup root directory for resolution of imports:
# the path of all local python libraries are relative to this
export PYTHONPATH=$SCRIPT_DIR/../

cd $SCRIPT_DIR/../

# Each command outputs the run directory, which is then used
# by the next command to add parameters and generate information
# used by the simulator. For a list of available options for each
# command, run 'export PYTHONPATH=.; <command> --help'.
setup/meta.py \
  --script $SCRIPT_DIR/`basename "$0"` \
  --results-directory $SCRIPT_DIR/all \
  --log INFO |
setup/dataset.py \
  --train-examples-per-class 20000 20000 20000 20000 20000 20000 20000 20000 20000 20000 \
  --name emnist-digits |
setup/nodes/google-fl.py \
  --nb-nodes 100 \
  --local-shards 2 \
  --shard-size 1000 |
setup/topology/ring.py \
  --metric random |
setup/model/linear.py |
simulate/algorithm/d_sgd.py \
  --learning-rate 0.1 \
  --batch-size 125 |
simulate/logger.py \
  --accuracy-logging-interval 1\
  --skip-testing \
  --nb-processes 1 |
simulate/run.py \
  --nb-epochs 1
