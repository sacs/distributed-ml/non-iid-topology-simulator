import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import argparse
import os
import json
import sys
import math

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Plot Local Accuracy Change Per Step')

    parser.add_argument('--results', type=str, nargs='+', default=None,
                    help='experiment result')
    parser.add_argument('--save-figure', type=str, default=None,
                    help='File in which to save figure.')
    parser.add_argument('--ymin', type=float, default=None,
                    help='Minimum value on the y axis')
    parser.add_argument('--ymax', type=float, default=None,
                    help='Maximum value on the y axis (data dependent).')
    parser.add_argument('--labels', type=str, nargs='+', default=[],
                    help='Labels that will appear in the legend')
    parser.add_argument('--legend', type=str, default='best',
                    help='Position of legend (default: best).')
    parser.add_argument('--no-legend', action='store_const', const=True, default=False,
                    help='Do not display legend (default: False).')
    parser.add_argument('--font-size', type=int, default=16,
                    help='Font size (default: 16).')
    parser.add_argument('--markers', type=str, nargs='+', default=[],
                    help='Markers used for each curve')
    parser.add_argument('--linestyles', type=str, nargs='+', default=[],
                    help='Linestyles used for each curve')
    parser.add_argument('--linewidth', type=float, default=1.5,
                    help='Line width of plot lines.')
    parser.add_argument('--node', type=int, default=None,
                    help='Node to plot (default: average of all nodes).')
    parser.add_argument('--yaxis', type=str, default='raw',
                    help='Yaxis metric (default: raw)', choices=['raw', 'moving-average', 'historical'])

    args = parser.parse_args()
    print(args)

    if args.results is None:
        stdin = sys.stdin.readlines()
        args.results = [ line.replace('\n','') for line in stdin ]

    matplotlib.rc('font', size=args.font_size)

    results = {}
    for result in args.results:
        with open(os.path.join(result, 'params.json'), 'r') as param_file:
            params = json.load(param_file)
        results[result] = { 
            "params": params, 
            "average": [], 
            "minimum": [], 
            "maximum": [],
            "nodes": { r:[] for r in range(params['nodes']['nb-nodes']) },
            "variation": { r: [] for r in range(params['nodes']['nb-nodes']) }
        }
        batch_nb = math.inf
        for rank in results[result]["nodes"].keys():
            with open(os.path.join(result, 'events', '{}.jsonlines'.format(rank)), 'r') as event_file:
                train_accuracy = results[result]["nodes"][rank]
                for line in event_file:
                    event = json.loads(line)
                    if event["type"] == "accuracy" and event["data"] == "train":
                        train_accuracy.append(event)
                train_accuracy.sort(key=lambda e: e["batch"])

                print(len(train_accuracy))
                batch_nb = batch_nb if batch_nb < len(train_accuracy) else len(train_accuracy)

                variation = results[result]["variation"][rank] 
                for i in range(len(train_accuracy)-1):
                    e1 = train_accuracy[i]["accuracy"]
                    e2 = train_accuracy[i+1]["accuracy"]
                    variation.append(e2-e1)

                if args.yaxis != 'raw':
                    if args.yaxis == 'moving-average':
                        avg = []
                        s = 0.0
                        m = 0.975 # Make an exponentially decreasing window
                        for i in range(len(variation)):
                            s = m*(variation[i] + s) 
                            avg.append(s/39.0)
                    elif args.yaxis == 'historical':
                        avg = [] 
                        for i in range(len(variation)):
                            s = sum(variation[:i])
                            avg.append(s/(i+1))
                    # Replace raw variations with moving average
                    results[result]["variation"][rank] = avg

        
        for b in range(batch_nb-1):
            var = [ results[result]["variation"][rank][b] for rank in range(params['nodes']['nb-nodes']) ]
            results[result]['average'].append(sum(var) / len(var))
            results[result]['minimum'].append(min(var))
            results[result]['maximum'].append(max(var))

    experiment_names = [ os.path.split(result)[1] if result[-1] != '/' else os.path.split(result[:-1])[1] for result in results ]

    fig, ax = plt.subplots()
    curves = []

    if len(args.labels) == 0:
        labels = experiment_names
    elif len(args.labels) < len(experiment_names):
        print('Insufficient number of labels')
        sys.exit(1)
    else:
        labels = args.labels

    if len(args.markers) == 0:
        markers = [ '' for _ in experiment_names ]
    elif len(args.markers) < len(experiment_names):
        print('Insufficient number of markers')
        sys.exit(1)
    else:
        markers = args.markers

    if len(args.linestyles) == 0:
        linestyles = [ '-' for _ in experiment_names ]
    elif len(args.linestyles) < len(experiment_names):
        print('Insufficient number of linestyles')
        sys.exit(1)
    else:
        linestyles = args.linestyles

    exps = [ k for k in results.keys() ]

    for name,marker,linestyle,result in zip(labels, markers,linestyles,exps):
        if not args.node is None:
            ydata = [ r*100 for r in results[result]['variation'][args.node] ]
        else:
            ydata = [ r*100 for r in results[result]['average'] ]
        curve = ax.plot(
          [ b for b in range(len(ydata)) ],
          ydata,
          label='{}'.format(name),
          marker=marker,
          linestyle=linestyle
        )

    # Add some text for batch-sizes, title and custom x-axis tick labels, etc.
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.spines['bottom'].set_color('#DDDDDD')
    ax.tick_params(bottom=False, left=False)
    ax.set_axisbelow(True)
    ax.yaxis.grid(True, color='#EEEEEE')
    ax.xaxis.grid(False)

    if not args.ymin is None:
        ax.set_ylim(bottom=args.ymin)

    if args.ymax is not None:
        ax.set_ylim(top=args.ymax)

    if args.yaxis == "raw":
        ax.set_ylabel("Accuracy Variation (%)")
    elif args.yaxis == "moving-average":
        ax.set_ylabel("Accuracy Variation (moving-average, %)")

    ax.set_xlabel('Steps')

    if not args.no_legend:
        ax.legend(loc=args.legend)

    fig.tight_layout()

    if args.save_figure is not None:
        plt.savefig(args.save_figure, transparent=True, bbox_inches='tight')
    plt.show()
